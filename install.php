#!/usr/bin/php7.2
<?php

function install_php_extension( $name ) {
	if ( extension_loaded( $name ) ) {
		echo "Success! $name extension is loaded!" . PHP_EOL;
	} else {
		echo "Installing $name extension..." . PHP_EOL;
		shell_exec( "sudo apt install php7.2-$name > /dev/null 2>&1" );
		if ( strpos( shell_exec(PHP_BINDIR . '/php7.2 -m' ), $name . PHP_EOL ) !== false ) {
			echo "Success! Script has installed $name extension successfully!" . PHP_EOL;
		} else {
			echo "Error! Script are not able to install $name extension for you! Try to do it manually..." . PHP_EOL;
			exit( 1 );
		}
	}
}

function install_application( $name ) {
    $check_command = "dpkg -l | grep -oP 'ii\s+$name'";
    if ( shell_exec( $check_command ) ) {
	    echo "Success! $name application is installed!" . PHP_EOL;
    } else {
	    echo "Installing $name application..." . PHP_EOL;
	    shell_exec( "sudo apt install $name > /dev/null 2>&1" );
	    if ( shell_exec( $check_command ) ) {
		    echo "Success! Script has installed $name application successfully!" . PHP_EOL;
        } else {
		    echo "Error! Script are not able to install $name application for you! Try to do it manually..." . PHP_EOL;
		    exit( 1 );
        }
    }
}

echo 'Notifier required tools installation running...' . PHP_EOL . PHP_EOL;

if ( version_compare( phpversion(), '7.2', '<' ) ) {
	echo 'Error! The required version of php is 7.2.' . PHP_EOL;
	$user_choice = readline('Do you want to install it manually [y|n]: ');
	if ( ! strcasecmp( $user_choice,'y') ) {
		shell_exec( 'sudo apt install php7.2 php7.2-cli > /dev/null 2>&1' );
		$result = shell_exec( PHP_BINDIR . '/php7.2 --version' );
		if ( strpos( $result, 'PHP 7.2' ) !== false ) {
			echo 'Success! Script has installed PHP 7.2 successfully!' . PHP_EOL;
		}
	} else {
		echo 'Ok. Then please install this manually or run the installation with php7.2' . PHP_EOL;
		exit( 1 );
	}
}

install_php_extension( 'curl' );
install_php_extension( 'gd' );

// For the play command
install_application( 'sox' );

