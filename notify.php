#!/usr/bin/php7.2
<?php

require_once 'constants.php';
require_once 'functions.php';

$groups = get_gitlab_response( 'groups' );
$found_group = null;
	
if ( $groups ) {
	foreach ( $groups as $group ) {
		if ( $group['name'] == GITLAB_GROUP_NAME ) {
			$found_group = $group;
			break;
		}
	}
} else {
	notify_error( 'Your groups not found!' );
	end_script();
}

if ( $found_group ) {
	$projects = get_gitlab_response( "groups/{$found_group['id']}/projects" );
} else {
	notify_error( 'Your selected GITLAB group not found!' );
	end_script();
}

if ( $projects ) {
	$old_commits = [];
	$new_commits = [];
	notify_success( 'GitLab Notifier has been launched!' );
	while ( 1 ) {
		foreach ( $projects as $project ) {
			$id = $project['id'];
			$new_commits[$id] = get_gitlab_response(
				"projects/$id/repository/commits?since=" . urlencode( date( DATE_ISO8601, strtotime( '-1day' ) ) ) 
			);
			if ( array_key_exists( $id, $old_commits ) ) {
				if ( $new_commits[$id] ) {
					foreach ( $new_commits[$id] as $new_commit ) {
						if ( ! is_array( $new_commit ) ) {
							// Smth went wrong therefore we need to keep an old value
							$new_commits[$id] = $old_commits[$id];
							break;
						}
						$found = false;
						foreach ( $old_commits[$id] as $old_commit ) {
							if ( $old_commit['id'] == $new_commit['id'] ) {
								$found = true;
								break;
							}
						}
						if ( ! $found ) {
							
							if ( $project['avatar_url'] ) {
								$img_url = $project['avatar_url'];
							} elseif ( $found_group['avatar_url'] ) {
								$img_url = $found_group['avatar_url'];
							}
							
							if ( $img_url ?? null ) {
								$img_name = basename( $img_url );
								$img_content = get_gitlab_response( $img_url, false );
								if ( $img_content ) {
									$img_path = PATH_TMP . "/$img_name";
									if ( ! file_put_contents( $img_path,  $img_content ) || 
									     ! resize_image( $img_path, 100, 100 ) ) {
										$img_path = null;
									}
								}
							}
							
							$message = 'Author: ' . quotes( $new_commit['author_name'] ) . '<br/>' 
								. 'Message: ' . quotes( $new_commit['message'] ) . '<br/>'
								. 'Project: ' . quotes( $project['name_with_namespace'] );
							
							notify_notify( $message, $img_path ?? PATH_IMAGES_SUCCESS );
						}
					}
				}
			}
		}
		$old_commits = $new_commits;
		foreach ( glob( PATH_TMP . '/*' ) as $tmp_file ) {
			unlink( $tmp_file );
		}
		sleep( NOTIFIER_SLEEP );
	}
} else {
	notify_error( 'Projects of the group not found!' );
	end_script();
}
