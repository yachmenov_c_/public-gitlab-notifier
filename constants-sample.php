<?php

/* Your private access token */
const GITLAB_ACCESS_TOKEN = '';
/* URL to the GitLab API */
const GITLAB_URL_PREFIX = 'https://gitlab.com/api/v4/';
/* A name of your GitLab group */
const GITLAB_GROUP_NAME = '';

const PATH_IMAGES = __DIR__ . '/images';
const PATH_IMAGES_ERROR = PATH_IMAGES . '/error.png';
const PATH_IMAGES_SUCCESS = PATH_IMAGES . '/success.png';

const PATH_SOUNDS = __DIR__ . '/sounds';
const PATH_SOUND_ERROR = PATH_SOUNDS . '/error.wav';
const PATH_SOUND_SUCCESS = PATH_SOUNDS . '/success.wav';

const PATH_TMP = __DIR__ . '/tmp';

/* The notification title */
const NOTIFIER_TITLE = 'Gitlab Notifier';
/* How long notifier will sleep before performing the next iteration (in seconds) */
const NOTIFIER_SLEEP = 60;
