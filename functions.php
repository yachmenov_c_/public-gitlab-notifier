<?php

/**
 * Check of the gitlab.com is reachable
 *
 * @return bool
 */
function is_connected() : bool {
    $connected = @fsockopen( 'gitlab.com', 443 );
    if ( $connected ){
        $is_conn = true;
        fclose( $connected );
    } else {
        $is_conn = false;
    }
    return $is_conn;
}

/**
 * Get cURL resource for the gitlab.com
 * 
 * @staticvar resource $ch cURL resource
 * 
 * @return resource
 */
function get_curl_resource() {
	static $ch = null;
	if ( is_null( $ch ) ) {
		$ch = curl_init();
		$headers = [
			'Private-Token: ' . GITLAB_ACCESS_TOKEN
		];
		curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	}
	
	return $ch;
}

/**
 * Maybe wait while the connection to gitlab.com are not restored...
*/
function maybe_wait_for_connection() : void {
	$was_not_connected = false;
	while ( ! is_connected() ) {
		$was_not_connected = true;
		$minutes = NOTIFIER_SLEEP / 60;
		notify_error( "Script can not connect to the gitlab.com!<br>Trying again in $minutes " . ( $minutes > 1 ? 'minutes.' : 'minute.' ) );
		sleep( NOTIFIER_SLEEP );
	}
	if ( $was_not_connected ) {
		notify_success( 'Connection has been restored!' );
	}
}

/**
 * Get response from the gitlab.com
 * 
 * @param string $url_affix
 * @param bool $use_as_affix
 * 
 * @return mixed
 */
function get_gitlab_response( string $url_affix, bool $use_as_affix = true ) {
	
	$ch = get_curl_resource();
	curl_setopt( $ch, CURLOPT_URL, $use_as_affix ? GITLAB_URL_PREFIX . $url_affix : $url_affix );

	maybe_wait_for_connection();
	
	$res = curl_exec( $ch );
	
	if ( ! $res ) {
		/* Failure */
		return $res;
	}
	
	$res_decoded = json_decode( $res, true );

	if ( json_last_error() === JSON_ERROR_NONE ) {
		/* JSON decoded */
		return $res_decoded;
	}
	
	/* Binary data or other different response */
	return $res;
}

/**
 * Terminate the program execution, because of error
*/
function end_script() {
	curl_close( get_curl_resource() );
	exit( 1 );
}

/**
 * Send to the shell a command to run a notification
 *
 * @param string $message
 * @param string $image
 * @param string $sound
 * @param string $title
 *
 * @access private
 */
function _notify( string $message, string $image, string $sound, string $title = NOTIFIER_TITLE ) : void {
	$command = "notify-send -i '$image' '$title' '$message' & play -q '$sound'  > /dev/null 2>&1";
	shell_exec( $command );
}

/**
 * Notify with error message
 *
 * @param string $message
 */
function notify_error( string $message ) : void {
	_notify( $message, PATH_IMAGES_ERROR, PATH_SOUND_ERROR );
}

/**
 * Notify with success message
 *
 * @param string $message
 */
function notify_success( string $message ) : void {
	_notify( $message, PATH_IMAGES_SUCCESS, PATH_SOUND_SUCCESS );
}

/**
 * Notify with a message and defined image
 *
 * @param string $message
 * @param string $image
 */
function notify_notify( string $message, string $image ) : void {
	_notify( $message, $image, PATH_SOUND_SUCCESS );
}

/**
 * Replace single quotes in a message with double quotes.
 *
 * @param string $message
 *
 * @return string
 */
function quotes( string $message ) : string {
	return str_replace( '\'', '"', $message );
}

/**
 * Resize an image via GD library.
 *
 * @param string $file
 * @param int $w
 * @param int $h
 *
 * @todo Rewrite to use the imagick lib.
 *
 * @return bool|null
 */
function resize_image( string $file, int $w, int $h ) : ?bool {
	list( $width, $height ) = getimagesize( $file );
	
	$ext = pathinfo( $file, PATHINFO_EXTENSION );
	switch ( $ext ) {
		case 'bmp': $ext = 'wbpm'; break;
		case 'png': break;
		case 'webp': break;
		case 'jpg': $ext = 'jpeg'; break;
		case 'jpeg': break;
		case 'gif': break;
		default: return null;
	}
	
	$src = @call_user_func( 'imagecreatefrom' . $ext, $file );
	if ( ! $src ) {
		return $src;
	}

	$dst = imagecreatetruecolor( $w, $h );
	
	// preserve transparency
	if ( $ext == 'gif' || $ext == 'png' ) {
		imagecolortransparent( $dst, imagecolorallocatealpha( $dst, 0, 0, 0, 127 ) );
		imagealphablending( $dst, false );
		imagesavealpha( $dst, true );
	}

	imagecopyresampled( $dst, $src, 0, 0, 0, 0, $w, $h, $width, $height );
	
	$res = call_user_func( 'image' . $ext, $dst, $file );
	
	imagedestroy( $src );
	imagedestroy( $dst );

	return $res;
}
